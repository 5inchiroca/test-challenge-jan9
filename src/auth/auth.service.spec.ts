import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './../users/users.service';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { hash } from './../utils/utils';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const UsersServiceMock = {
      provide: UsersService,
      useValue: {
        findOne: jest.fn().mockResolvedValue({
          password: await hash('12345'),
        }),
      },
    };
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [AuthService, LocalStrategy, JwtStrategy, UsersServiceMock],
    }).compile();

    service = moduleRef.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

describe('validateUser', () => {
  let service: AuthService;
  const pass = 'guess';

  beforeEach(async () => {
    const UsersServiceMock = {
      provide: UsersService,
      useValue: {
        findOne: jest.fn().mockResolvedValue({
          id: 3,
          password: await hash(pass),
        }),
      },
    };
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [AuthService, LocalStrategy, JwtStrategy, UsersServiceMock],
    }).compile();

    service = moduleRef.get<AuthService>(AuthService);
  });

  it('should return a user object when credentials are valid', async () => {
    const res = await service.validateUser('maria', pass);
    expect(res.id).toEqual(3);
  });

  it('should return null when credentials are invalid', async () => {
    const res = await service.validateUser('xxx', 'xxx');
    expect(res).toBeNull();
  });
});

describe('validateLogin', () => {
  let service: AuthService;

  beforeEach(async () => {
    const UsersServiceMock = {
      provide: UsersService,
      useValue: {
        findOne: jest.fn().mockResolvedValue({
          password: await hash('12345'),
        }),
      },
    };
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [AuthService, LocalStrategy, JwtStrategy, UsersServiceMock],
    }).compile();

    service = moduleRef.get<AuthService>(AuthService);
  });

  it('should return JWT object when credentials are valid', async () => {
    const res = await service.login({ username: 'maria', id: 3 });
    expect(res.access_token).toBeDefined();
  });
});
