import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { compare } from './../utils/utils';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(email);
    if (user && (await compare(pass, user.password))) {
      delete user.password;
      return user;
    }
    return null;
  }

  async login({ username, id }: any) {
    const payload = { username, sub: id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
