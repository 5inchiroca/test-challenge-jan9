import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1673265600676 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS words
      (
        id serial PRIMARY KEY,
        word character varying COLLATE pg_catalog."default" NOT NULL,
        is_active boolean NOT NULL DEFAULT true,
        selected boolean NOT NULL DEFAULT false,
        hits integer NOT NULL DEFAULT 0
      )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
