import { join } from 'path';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';
import * as accents from 'remove-accents';

export class test1673265600751 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const queryInsertBase = `insert into words(word`;
    const insertRecords = async (r) => {
      const values = r.map((w) => `('${w}')`).join(',');
      const query = `${queryInsertBase}) values ${values}`;
      return queryRunner.query(query);
    };
    const parseWord = (w) => {
      const lArr = [];
      for (let letter of w) {
        if (letter !== 'ñ') {
          letter = accents.remove(letter);
        }
        lArr.push(letter);
      }
      return lArr.join('');
    };
    const insertSelectedWord = (w) =>
      queryRunner.query(`${queryInsertBase}, selected) values ('${w}', true)`);
    let loaded = false,
      selectWord = true;
    let n = 1;
    const dictionary = {},
      chunk = [],
      promises = [],
      chunkSize = 10000,
      wordLength = 5;

    const file = join(__dirname, '../../../data/words.txt');

    await new Promise((res) => {
      const readStream = createReadStream(file, 'utf-8');
      const rl = createInterface({ input: readStream });
      rl.on('line', (word) => {
        n++;
        if (chunk.length === chunkSize) {
          promises.push(insertRecords(chunk));
          loaded = true;
          chunk.length = 0;
        }
        if (word.length === wordLength) {
          const parsedWord = parseWord(word);
          if (!dictionary[parsedWord]) {
            dictionary[parsedWord] = 1;
            if (selectWord) {
              promises.push(insertSelectedWord(parsedWord));
              selectWord = false;
            } else {
              chunk.push(parsedWord);
            }
          }
          loaded = false;
        }
      });
      rl.on('error', (error) => console.log(error.message));
      rl.on('close', async () => {
        console.log('Number of lines: ', n);
        if (!loaded) {
          promises.push(insertRecords(chunk));
        }
        console.log('Data parsing completed');
        await Promise.all(promises);
        res(1);
      });
    });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
