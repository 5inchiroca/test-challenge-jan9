import { hash } from './../../utils/utils';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1673265600839 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS users (
        id serial PRIMARY KEY,
        email character varying COLLATE pg_catalog."default" NOT NULL,
        username character varying COLLATE pg_catalog."default" NOT NULL,
        password character varying COLLATE pg_catalog."default" NOT NULL,
        games integer NOT NULL DEFAULT 0,
        attempts integer NOT NULL DEFAULT 0,
        games_won integer NOT NULL DEFAULT 0
      )`,
    );
    const hashedPassword = await hash('12345');
    await queryRunner.query(
      `insert into users (email, username, password) values ('admin@google.com', 'admin', '${hashedPassword}')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
