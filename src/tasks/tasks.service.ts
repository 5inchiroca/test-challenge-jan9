import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { WordsService } from './../words/words.service';
import { UsersService } from './../users/users.service';

@Injectable()
export class TasksService {
  constructor(
    private readonly wordsService: WordsService,
    private readonly usersService: UsersService,
  ) {}
  private readonly logger = new Logger(TasksService.name);

  @Cron('*/5 * * * *')
  async selectNewWord() {
    await this.wordsService.selectNewWord();

    await this.usersService.resetAttempts();

    this.logger.debug('Called every 5 minutes, new word selected!');
  }
}
