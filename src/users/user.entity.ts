import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column({ default: 0 })
  games: number;

  @Column({ default: 0 })
  attempts: number;

  @Column({ default: 0, name: 'games_won' })
  gamesWon: number;
}
