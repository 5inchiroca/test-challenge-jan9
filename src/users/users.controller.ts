import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/guards/jwt-auth.guard';
import { UsersService } from './users.service';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('top-players')
  async mostGuessed() {
    return await this.usersService.topPlayers();
  }

  @Get('stats/:username')
  async stats(@Param() { username }) {
    return await this.usersService.stats(username);
  }
}
