import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  findOne(username: string) {
    return this.usersRepository.findOne({ where: { username } });
  }

  async stats(username: string) {
    const user = await this.findOne(username);
    if (user) {
      return {
        username,
        games: user.games,
        gamesWon: user.gamesWon,
      };
    }
    throw new NotFoundException();
  }

  updateAttempts({ id, games, attempts }) {
    const updateQuery = { attempts: attempts + 1 };
    if (attempts === 0) {
      updateQuery['games'] = games + 1;
    }
    return this.usersRepository.update(id, updateQuery);
  }

  wonGame({ id, gamesWon }) {
    return this.usersRepository.update(id, { gamesWon: gamesWon + 1 });
  }

  topPlayers() {
    return this.usersRepository.find({
      order: {
        gamesWon: 'desc',
      },
    });
  }

  resetAttempts() {
    return this.usersRepository.update({}, { attempts: 0 });
  }
}
