import * as bcrypt from 'bcrypt';

const saltOrRounds = 10;

export const hash = (password: string) => bcrypt.hash(password, saltOrRounds);

export const compare = (password: string, hash: string) =>
  bcrypt.compare(password, hash);
