import { Length } from 'class-validator';

export class ValidateWordDto {
  @Length(5, 5)
  word: string;
}
