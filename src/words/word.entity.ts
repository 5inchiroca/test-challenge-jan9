import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('words')
export class Word {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  word: string;

  @Column({ default: true, name: 'is_active' })
  isActive: boolean;

  @Column({ default: false })
  selected: boolean;

  @Column({ default: 0 })
  hits: number;
}
