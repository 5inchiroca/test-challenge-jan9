import { Test, TestingModule } from '@nestjs/testing';
import { WordsController } from './words.controller';
import { WordsService } from './words.service';

describe('WordsController', () => {
  let controller: WordsController;
  let wordsService: WordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WordsController],
      providers: [
        {
          provide: WordsService,
          useValue: {
            validate: jest.fn().mockResolvedValue([
              {
                letter: 'g',
                value: 1,
              },
              {
                letter: 'a',
                value: 2,
              },
              {
                letter: 't',
                value: 3,
              },
              {
                letter: 'o',
                value: 1,
              },
              {
                letter: 's',
                value: 2,
              },
            ]),
            mostGuessed: jest.fn().mockResolvedValue([
              {
                id: 1,
                word: 'abadi',
                isActive: false,
                selected: false,
                hits: 5,
              },
              {
                id: 2,
                word: 'racel',
                isActive: false,
                selected: false,
                hits: 1,
              },
            ]),
          },
        },
      ],
    }).compile();

    controller = module.get<WordsController>(WordsController);
    wordsService = module.get<WordsService>(WordsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should validate word', async () => {
    const word = 'gatos';
    const user = { id: 1 };
    await controller.validate({ user }, { word });
    expect(wordsService.validate).toBeCalledWith(user, word);
  });

  it('should return most guessed words', async () => {
    await controller.mostGuessed();
    expect(wordsService.mostGuessed).toHaveBeenCalled();
  });
});
