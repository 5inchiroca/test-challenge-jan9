import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/guards/jwt-auth.guard';
import { ValidateWordDto } from './dto/validate-word.dto';
import { WordsService } from './words.service';

@UseGuards(JwtAuthGuard)
@Controller('words')
export class WordsController {
  constructor(private readonly wordsService: WordsService) {}
  @Post('validate')
  async validate(@Req() { user }, @Body() { word }: ValidateWordDto) {
    return await this.wordsService.validate(user, word);
  }

  @Get('most-guessed')
  async mostGuessed() {
    return await this.wordsService.mostGuessed();
  }
}
