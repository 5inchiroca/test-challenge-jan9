import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Word } from './word.entity';
import { WordsService } from './words.service';
import { WordsController } from './words.controller';
import { UsersModule } from './../users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([Word]), UsersModule],
  providers: [WordsService],
  controllers: [WordsController],
  exports: [WordsService],
})
export class WordsModule {}
