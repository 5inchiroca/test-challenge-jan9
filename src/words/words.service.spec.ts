import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { WordsService } from './words.service';
import { Word } from './word.entity';
import { UsersService } from './../users/users.service';
import { MoreThan, Repository } from 'typeorm';

const wordArray = [
  {
    id: 5,
    word: 'silla',
    isActive: false,
    selected: false,
    hits: 10,
  },
  {
    id: 11,
    word: 'balon',
    isActive: false,
    selected: false,
    hits: 6,
  },
];

const selectedWord = {
  id: 3,
  word: 'barco',
  isActive: true,
  selected: true,
  hits: 0,
};

describe('WordsService', () => {
  let service: WordsService;
  let repository: Repository<Word>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WordsService,
        {
          provide: getRepositoryToken(Word),
          useValue: {
            findOne: jest.fn().mockResolvedValue(selectedWord),
            find: jest.fn().mockResolvedValue(wordArray),
          },
        },
        { provide: UsersService, useValue: {} },
      ],
    }).compile();

    service = module.get<WordsService>(WordsService);
    repository = module.get<Repository<Word>>(getRepositoryToken(Word));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should get selected word', async () => {
    const repoSpy = jest.spyOn(repository, 'findOne');
    const { selected } = await service.getSelectedWord();
    expect(selected).toBe(true);
    expect(repoSpy).toBeCalledWith({ where: { selected: true } });
  });

  it('should return most guessed words', async () => {
    const repoSpy = jest.spyOn(repository, 'find');
    const mostGuessed = await service.mostGuessed();
    let maxHits;
    mostGuessed.forEach((w, i) => {
      if (i === 0) {
        maxHits = w.hits;
      }
      expect(w.hits).toBeGreaterThan(0);
      expect(w.hits).toBeLessThanOrEqual(maxHits);
      maxHits = w.hits;
    });
    expect(repoSpy).toBeCalledWith({
      where: [{ hits: MoreThan(0) }],
      order: { hits: 'desc' },
      take: 10,
    });
  });
});
