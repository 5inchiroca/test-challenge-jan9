import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoreThan, Repository } from 'typeorm';
import * as accents from 'remove-accents';
import { Word } from './word.entity';
import { User } from './../users/user.entity';
import { UsersService } from './../users/users.service';

@Injectable()
export class WordsService {
  constructor(
    @InjectRepository(Word)
    private readonly wordsRepository: Repository<Word>,
    private readonly usersService: UsersService,
  ) {}
  private readonly logger = new Logger(WordsService.name);

  async selectNewWord() {
    const selectedWord = await this.getSelectedWord();

    await this.wordsRepository.update(selectedWord.id, {
      isActive: false,
      selected: false,
    });

    const [newSelectedWord] = await this.wordsRepository.query(
      `select * from words where is_active = true order by random() limit 1`,
    );

    await this.wordsRepository.update(newSelectedWord.id, {
      selected: true,
    });

    this.logger.debug('Selected word', newSelectedWord);
  }

  getSelectedWord(): Promise<Word> {
    return this.wordsRepository.findOne({
      where: { selected: true },
    });
  }

  async mostGuessed() {
    return this.wordsRepository.find({
      where: [{ hits: MoreThan(0) }],
      order: { hits: 'desc' },
      take: 10,
    });
  }

  async validate(user: User, word: string) {
    if (user.attempts === 5) {
      throw new BadRequestException('You have reached the limit of attempts');
    }

    await this.usersService.updateAttempts(user);

    const { id, word: selectedWord, hits } = await this.getSelectedWord();

    const res = [];
    let rightGuess = true;

    for (let i = 0; i < word.length; i++) {
      let letter = word[i].toLocaleLowerCase();
      const or_letter = word[i];
      let selectedLetter = selectedWord[i];
      if (letter !== 'ñ') {
        letter = accents.remove(letter);
      }
      if (selectedLetter !== 'ñ') {
        selectedLetter = accents.remove(selectedLetter);
      }
      let value;
      if (letter === selectedLetter) {
        value = 1;
      } else {
        value = selectedWord.includes(letter) ? 2 : 3;
        rightGuess = false;
      }
      res.push({ letter: or_letter, value });
    }

    if (rightGuess) {
      await this.wordsRepository.update(id, { hits: hits + 1 });

      await this.usersService.wonGame(user);
    }

    return res;
  }
}
